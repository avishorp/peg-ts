module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  "setupFilesAfterEnv": [ "./test/assertions.ts" ]
};