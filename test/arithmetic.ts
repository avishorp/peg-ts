import * as ast from '../src/ast';
import * as compiler from '../src/compiler';

const arithmeticGrammar: ast.Grammar = [
    // Expression = head:Term tail:(_ @("+" / "-") _ @Term)*
    ast.rule(
        'Expression', 
        'number',
        ast.topLevel(
            [ ast.ruleRef('Term'), 'head'],
            [ ast.zeroOrMore(
                ast.sequence(
                    ast.ruleRef('_'),
                    ast.choice(ast.literal("+"), ast.literal("-")),
                    ast.ruleRef('_'),
                    ast.ruleRef('Term')
                )
            ), 'tail']
        ),
        // Code
        `
        return tail.reduce(function(result, element) {
            if (element[1] === "+") return result + element[3]
            else /* element[1] === "-" */
                return result - element[3];
          }, head);        
        `),

    // Term = head:Factor tail:(_ @("*" / "/") _ @Factor)* {
    ast.rule(
        'Term', // Rule Name
        'number', // Result Type
        ast.topLevel(
            [ ast.ruleRef('Factor'), 'head' ],
            [ ast.zeroOrMore(
                ast.sequence(
                    ast.ruleRef('_'),
                    ast.choice(ast.literal("*"), ast.literal("/")),
                    ast.ruleRef('_'),
                    ast.ruleRef('Factor')
                )), 'tail']
        ),
        // Code
        `
        return tail.reduce(function(result, element) {
            if (element[1] === "*") return result * element[3]
            else /* element[1] === "/" */
                return result / element[3];
          }, head);    
        `
        ),

        // Factor = "(" _ Expression _ ")" / Integer
        ast.rule(
            'Factor', // Rule Name
            'number', // Rule result type
            ast.topLevel(
                [ ast.choice(
                    ast.sequence(
                        ast.literal('('),
                        ast.ruleRef('_'),
                        ast.ruleRef('Expression'),
                        ast.ruleRef('_'),
                        ast.literal(')')
                    ),
                    ast.ruleRef('Integer')
                )]
            ),
            'if (Array.isArray(arg0)) return arg0[2]; else return arg0;'
        ),

        // Integer = _ [0-9]+
        ast.rule(
            'Integer', // Rule Name
            'number', // Result Type
            ast.topLevel(
                [ ast.ruleRef('_') ],
                [ ast.oneOrMore(ast.charClass('0-9')), 'n']
            ),
            `return parseInt(n.join(''), 10);`
        ),

        // _ = [ \t\b\r]*
        ast.rule(
            '_',
            'void',
            ast.topLevel(
                [ ast.zeroOrMore(ast.charClass(' \\t\\n\\r')) ]
            )
        )
]



const p = compiler.compile(arithmeticGrammar, { noFormat: true });
console.log(p)
