import * as ast from "./ast";
import { format } from 'prettier';
import * as uniq from 'uniq';

interface CompileOptions {
    noFormat?: boolean;
    embedRuntime?: boolean;
}

const DEFAULT_COMPILER_OPTIONS: CompileOptions = {
    noFormat: false,
    embedRuntime: false
}

type TypeMap = { [ ruleName: string ]: string[] };

function inferGrammarTypes(grammar: ast.Grammar): TypeMap {

    function unionType(parts: string[]): string {
        const p = [...parts];
        uniq(p);
        return p.join('|');
    }

    function findRule(ruleName: string): ast.RuleNode {
        const r = grammar.find(n => n.name === ruleName);
        if (!r)
            throw Error(`Reference to undefined rule ${ast.rule}`);
        return r;
    }

    function nodeType(node: ast.ASTNode): string {
        switch(node.type) {
            case 'char':
            case 'literal':
            case 'any':
                return 'string';

            case 'sequence':
                return '[' + (node as ast.SequenceNode).children.map(child => nodeType(child)).join(',') + ']';

            case 'choice':
                return unionType((node as ast.ChoiceNode).children.map(child => nodeType(child)))

            case 'zeroOrMore':
            case 'oneOrMore':
                return nodeType((node as ast.ZeroOrMoreNode).child) + '[]';

            case 'ruleRef':
                return findRule((node as ast.RuleRefNode).ruleName).assignedType;

            default:
                throw Error('Invalid node type');
        }
    }

    const typeMap: TypeMap = {};
    grammar.forEach(rule => {
        typeMap[rule.name] = rule.child.children.map(entry => nodeType(entry[0]));
    })
    return typeMap;
}

export function compile(grammar: ast.Grammar, options?: CompileOptions): any {

    function mkIndent(count: number): string {
        return Array(count).join(' ');
    }

    function prolog() {
        return [ `import { ParseResult, matchClass, matchLiteral, matchAny, matchSequence, matchChoice, matchZeroOrMore, matchOneOrMore, matchRule, parserWrapper } from '../packages/peg-ts-runtime'` ];
    }

    function epilog() {
        return [];
    }

    function compileGrammar(grammar: ast.Grammar): string[] {

        const genCode: string[] = []
        const userCode: string[] = []
        const typeMap = inferGrammarTypes(grammar);

        grammar.forEach(ruleNode => {
            const [ ruleGenCode, ruleUserCode ] = compileRule(ruleNode, typeMap);
            genCode.push(ruleGenCode);
            if (ruleUserCode)
                userCode.push(ruleUserCode);
        });

        const parsers = grammar.map(ruleNode => 
            `export const parse${ruleNode.name} = (input: string, pos: number = 0) => parserWrapper(matchRule(match${ruleNode.name}, _user${ruleNode.name}), input, pos);`
        );

        return [
            `// Matchers`,
            ...genCode,
            ``,
            `// User Code`,
            ...userCode,
            ``,
            `// Parsers`,
            ...parsers
        ]
    }

    function compileRule(node: ast.RuleNode, typeMap: TypeMap): [string, string|null] {
        const { name, child, code } = node;

        const argTypes = typeMap[node.name];
        const userFnArguments = child.children.map(([_, name], index) => `${name || `arg${index}`}: ${argTypes[index]}`)
            .join(', ');

        const matcher = `function match${name}(input: string, pos: number): ParseResult<[${argTypes.join(',')}]> { return ${compileNode(child)}(input, pos); };`
        const userCode = code?
            `function _user${name}(${userFnArguments}): ${node.assignedType} { ${code} }` :
            `function _user${name}(${userFnArguments}): ${node.assignedType} {}`;
        
        return [ matcher, userCode ];
    }

    function compileNode(node: ast.ASTNode): string {

    
        switch(node.type) {
            case 'char':
                return `matchClass(/[${(node as ast.CharClassNode).value.split('').map(c => c).join('')}]/)`;

            case 'literal':
                return `matchLiteral('${(node as ast.Literal).value}')`;

            case 'any':
                return `matchAny()`;

            case 'sequence':
                return `matchSequence(` + (node as ast.SequenceNode).children.map(compileNode)+ `)`;

            case 'topLevel':
                return `matchSequence(` + (node as ast.TopLevelNode).children.map(n => compileNode(n[0]))+ `)`;

            case 'choice':
                return `matchChoice(` + (node as ast.ChoiceNode).children.map(compileNode)+ `)`;

            case 'zeroOrMore':
                return `matchZeroOrMore(` + compileNode((node as ast.ZeroOrMoreNode).child) + `)`;

            case 'oneOrMore':
                return `matchOneOrMore(` + compileNode((node as ast.OneOrMoreNode).child) + `)`;
    

            case 'ruleRef':
                const ruleName = (node as ast.RuleRefNode).ruleName;
                return `matchRule(match${ruleName}, _user${ruleName})`

            default:
                console.log("---" + node.type)
                throw new Error(`Unknown AST Node type ${node.type}`);
        }

    }


    // Compile every rule
    const program = [
        ...prolog(),
        ...compileGrammar(grammar),
        ...epilog()
    ];

    const {  noFormat, embedRuntime } = { ...DEFAULT_COMPILER_OPTIONS, ...(options || {}) };
    const rawProgram = program.join('\n');

    return noFormat? rawProgram : format(rawProgram);

}

