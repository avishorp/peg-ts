
export interface ASTNode {
    type: string;
}

// Match a single character with one of the
// characters in the options.
export interface CharClassNode extends ASTNode {
    type: 'char';
    value: string;
}

export interface Literal extends ASTNode {
    type: 'literal',
    value: string
}

// Match any character
export interface AnyNode extends ASTNode {
    type: 'any';
}

// Match a sequence
export interface SequenceNode extends ASTNode {
    type: 'sequence';
    children: Array<ASTNode>;
}

// Match any of the choices
export interface ChoiceNode extends ASTNode {
    type: 'choice';
    children: Array<ASTNode>;
}

// Match if the expression is not matched
export interface NotNode extends ASTNode {
    type: 'not';
    child: ASTNode;
}

// Match zero or more occurances of the expression
export interface ZeroOrMoreNode extends ASTNode {
    type: 'zeroOrMore';
    child: ASTNode;
}

// Match one or more occurances of the expression
export interface OneOrMoreNode extends ASTNode {
    type: 'oneOrMore';
    child: ASTNode;
}

// Match a rule whose name is given
export interface RuleNode extends ASTNode {
    type: 'rule';
    name: string;
    assignedType: string;
    code?: string;
    child: TopLevelNode;
}

export interface TopLevelNode extends ASTNode {
    type: 'topLevel';
    children: Array<[ASTNode, string?]>;
}

export interface RuleRefNode extends ASTNode {
    type: 'ruleRef';
    ruleName: string;
}

export interface Squelched extends ASTNode {
    type: 'squelched',
    child: ASTNode;
}

export type Grammar = Array<RuleNode>;


export const charClass = (value: string): CharClassNode => ({ type: 'char', value });

export const literal = (value: string): Literal => ({ type: 'literal', value })

export const any = (): AnyNode => ({ type: 'any' });

export const sequence = (...children: Array<ASTNode>): SequenceNode => ({ type: 'sequence', children });

export const choice = (...children: Array<ASTNode>): ChoiceNode => ({ type: 'choice', children });

export const not = (child: ASTNode): NotNode => ({ type: 'not', child });

export const zeroOrMore = (child: ASTNode): ZeroOrMoreNode => ({ type: 'zeroOrMore', child });

export const oneOrMore = (child: ASTNode): OneOrMoreNode => ({ type: 'oneOrMore', child });

export const rule = (name: string, assignedType: string, child: TopLevelNode, code?: string): RuleNode => ({ type: 'rule', name, assignedType, child, code });

export const topLevel = (...children: Array<[ASTNode, string?]>): TopLevelNode => ({ type: 'topLevel', children });

export const ruleRef = (ruleName: string) => ({ type: 'ruleRef', ruleName });
