import * as dut from '../src';


test('Literal', () => {
    const p = dut.matchLiteral('abc');
    expect(p('abc', 0)).toEqual({ fail: false, value: 'abc', inc: 3 });
    expect(p('abx', 0)).toEqual({ fail: true, inc: 0 });
});

test('Class', () => {
    const p = dut.matchClass(/[a-z]/);
    expect(p('a', 0)).toEqual({ fail: false, value: 'a', inc: 1 });
    expect(p('A', 0)).toEqual({ fail: true, inc: 0 });
    expect(p('a', 1)).toEqual({ fail: true, inc: 0 });
    
});

test('Sequence', () => {
    const p = dut.matchSequence(
        dut.matchClass(/[a-z]/),
        dut.matchClass(/[0-9]/)
        );

    expect(p('a0', 0)).toEqual({ fail: false, value: ['a', '0'], inc: 2 });
    expect(p('00', 0)).toEqual({ fail: true, inc: 0 });
    expect(p('ab', 0)).toEqual({ fail: true, inc: 1 });
    expect(p('a', 0)).toEqual({ fail: true, inc: 1 });
});

test('Choice', () => {
    const p = dut.matchChoice(
        dut.matchClass(/[0-9]/),
        dut.matchLiteral('hello')
    );
    expect(p('5', 0)).toEqual({ fail: false, value: '5', inc: 1 });
    expect(p('hello', 0)).toEqual({ fail: false, value: 'hello', inc: 5 });
    expect(p('world', 0)).toEqual({ fail: true, inc: 0 });
});

test('Zero or More', () => {
    const p = dut.matchZeroOrMore(dut.matchClass(/[0-9]/));
    expect(p('', 0)).toEqual({ fail: false, value: [], inc: 0 });
    expect(p('a', 0)).toEqual({ fail: false, value: [], inc: 0 });
    expect(p('0', 0)).toEqual({ fail: false, value: ['0'], inc: 1 });
    expect(p('a012', 1)).toEqual({ fail: false, value: ['0', '1', '2'], inc: 3 });
});

test('One or More', () => {
    const p = dut.matchOneOrMore(dut.matchLiteral('ab'));
    expect(p('ab', 0)).toEqual({ fail: false, value: ['ab'], inc: 2 });
    expect(p('abab', 0)).toEqual({ fail: false, value: ['ab', 'ab' ], inc: 4});
    expect(p('a', 0)).toEqual({ fail: true, inc: 0 })
});

test('Composition', () => {
    const p = dut.matchSequence(
        dut.matchOneOrMore(dut.matchClass(/[0-9]/)),
        dut.matchChoice(dut.matchLiteral('+'), dut.matchLiteral('-')),
        dut.matchOneOrMore(dut.matchClass(/[0-9]/))
    );
    expect(p('12+13', 0)).toEqual({ fail: false, value: [['1', '2'], '+', ['1', '3']], inc: 5});
    expect(p('2544-17', 0)).toEqual({ fail: false, value: [['2', '5', '4', '4'], '-', ['1', '7']], inc: 7});
    expect(p('12*12', 0)).toEqual({fail: true, inc: 2 });
});


