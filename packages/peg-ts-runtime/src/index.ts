
export interface ParseResult<T> {
    fail: boolean;
    value?: T;
    inc: number;
}

export function matchLiteral(literal: string): (input: string, pos: number) => ParseResult<string> {
    return (input: string, pos: number) => {
    if (input.slice(pos).startsWith(literal))
        return {
            fail: false,
            value: literal,
            inc: literal.length
        }
    else 
        return {
            fail: true,
            inc: 0
        };   
    }
}

export function matchClass(classDef: RegExp): (input: string, pos: number) => ParseResult<string> {
    return (input: string, pos: number) => {

    if (classDef.test(input.charAt(pos)))
        return {
            fail: false,
            value: input.charAt(pos),
            inc: 1
        }
    else
        return {
            fail: true,
            inc: 0
        };
    }
}

export function matchAny(): (input: string, pos: number) => ParseResult<string> {
    return (input: string, pos: number) =>
        (pos < input.length)?
            {
                fail: false,
                value: input[pos],
                inc: 1
            }:
            {
                fail: true,
                inc: 0
            }
}

type SequenceResult<T extends ReadonlyArray<(input: string, pos: number) => ParseResult<any>>> =
    { [K in keyof T]: T[K] extends (input: string, pos: number) => ParseResult<infer V>? V : never };
export function matchSequence<T extends ReadonlyArray<(input: string, pos: number) => ParseResult<any>>>(...elements: T):  (input: string, pos: number) => ParseResult<SequenceResult<T>> {
    return (input: string, pos: number) => {
    
        return elements.reduce<any>((result, e) => {
            if (result.fail)
                return result;
            else {
                const r = e(input, pos + result.inc)
                if (r.fail)
                    return {
                        fail: true,
                        inc: result.inc
                    }
                else return {
                    fail: false,
                    value: [ ... result.value, r.value ],
                    inc: result.inc + r.inc
                }
            }
        },
            {
                fail: false,
                inc: 0,
                value: []
            }
        ) as any;
    }
}

type ReturnType<T> = T extends (...args: any) => infer A ? A : never;
export function matchChoice<T extends ReadonlyArray<(input: string, pos: number) => ParseResult<unknown>>>(...elements: T): (input: string, pos: number) => ReturnType<T[number]> {

    return (input: string, pos: number) => {
  
        return elements.reduce<any>((result, e) => {
            if (!result.fail)
                return result;
            else {
                const r = e(input, pos);
                if (r.fail)
                    return result ;
                else
                    return r;
            }
        },
        {
            fail: true,
            inc: 0
        } as ParseResult<any>)
    }
} 

type ReturnParseResultType<T> = T extends (...args: any) => ParseResult<infer A> ? A : never;
export function matchZeroOrMore<T extends (input: string, pos: number) => ParseResult<any>>(element: T): (input: string, pos: number) => ParseResult<Array<ReturnParseResultType<T>>> {
    return (input: string, pos: number) => {
        const result: Array<ReturnParseResultType<T>> = [];
        let inc = 0;

        while(true) {
            const r = element(input, pos + inc);
            if (r.fail)
                break;
            else {
                result.push(r.value);
                inc += r.inc;
            }
        }

        return {
            fail: false,
            inc: inc,
            value: result
        };
    }
}

export function matchOneOrMore<T extends (input: string, pos: number) => ParseResult<any>>(element: T): (input: string, pos: number) => ParseResult<Array<ReturnParseResultType<T>>> {
    return (input: string, pos: number) => {
        const r = matchZeroOrMore(element)(input, pos);
        if ((r.value!).length > 0)
            return r;
        else
            return {
                fail: true,
                inc: 0
            }
    }
    
}

export function matchRule<R>(matcher: (input: string, pos: number) => ParseResult<ReadonlyArray<any>>, userFunc: (...args: ReadonlyArray<any>) => R) {
    return (input: string, pos: number) => {
        const p = matcher(input, pos);
        if (p.fail)
            return { ...p } as ParseResult<R>;
        else 
            return {
                fail: false,
                inc: p.inc,
                value: userFunc(...(p.value!))
            } as ParseResult<R>
    }
}

export function parserWrapper<T>(matcher: (input: string, pos: number) => ParseResult<T>, input: string, pos: number): T {
    const r = matcher(input, pos);
    if (r.fail)
        throw Error("Parse error");
    else
        return r.value!;
}

